webpackJsonp([0],{

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeoplePageModule", function() { return PeoplePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__people__ = __webpack_require__(295);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PeoplePageModule = /** @class */ (function () {
    function PeoplePageModule() {
    }
    PeoplePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__people__["a" /* PeoplePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__people__["a" /* PeoplePage */]),
            ],
        })
    ], PeoplePageModule);
    return PeoplePageModule;
}());

//# sourceMappingURL=people.module.js.map

/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { PeoplePage } from '../people/people';

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetailsPage = /** @class */ (function () {
    function DetailsPage(navCtrl, navParams, loadingCtrl, storage, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.peoples = [];
        this.key = 'peoples';
        this.people = this.navParams.get('people');
    }
    DetailsPage.prototype.deleteConfirm = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Xoá nhân viên',
            message: "B\u1EA1n c\u00F3 th\u1EF1c s\u1EF1 mu\u1ED1n xo\u00E1 nh\u00E2n vi\u00EAn kh\u1ECFi danh s\u00E1ch ?",
            buttons: [
                {
                    text: 'Không',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Có',
                    handler: function () {
                        _this.presentLoading();
                        _this.deletePeople();
                        console.log('Agree clicked');
                    }
                }
            ]
        });
        confirm.present();
    };
    DetailsPage.prototype.presentLoading = function () {
        var loader = this.loadingCtrl.create({
            content: "Vui lòng chờ ...",
            duration: 2000
        });
        loader.present();
    };
    DetailsPage.prototype.deletePeople = function () {
        var _this = this;
        this.storage.get(this.key).then(function (value) {
            if (value != null && value != undefined) {
                _this.peoples = JSON.parse(value);
                for (var i = 0; i < _this.peoples.length; i++) {
                    if (_this.peoples[i].id === _this.people.id) {
                        _this.peoples.splice(i, 1);
                    }
                }
                _this.storage.set(_this.key, JSON.stringify(_this.peoples));
            }
        });
        this.navCtrl.pop();
        //this.navCtrl.parent.select(0);
    };
    DetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailsPage');
    };
    DetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-details',template:/*ion-inline-start:"/Users/huutoan94/Ionic_projects/ionicTraining1/toan_training_project/src/pages/details/details.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons start>\n            <button ion-button menuToggle>\n              <ion-icon name="menu"></ion-icon>\n            </button>\n          </ion-buttons>\n          <ion-title>Details</ion-title>\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content>\n    <ion-card>\n      <img src=""/>\n      <ion-card-content *ngIf="people!=null">\n        <ion-card-title>\n          {{people.employee_name}}\n          </ion-card-title>\n        <p>\n          {{people.employee_salary}}\n        </p>\n      </ion-card-content>\n      <button padding ion-button color="light" round (click)=\'deleteConfirm()\'>Xoá nhân viên</button>\n    </ion-card>\n  </ion-content>'/*ion-inline-end:"/Users/huutoan94/Ionic_projects/ionicTraining1/toan_training_project/src/pages/details/details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], DetailsPage);
    return DetailsPage;
}());

//# sourceMappingURL=details.js.map

/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(201);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MapPage = /** @class */ (function () {
    function MapPage(navCtrl, navParams, http, geo, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.geo = geo;
        this.storage = storage;
        this.provinces = [];
        this.province = {};
        this.dist = {};
        this.urlProvinces = 'http://222.255.102.157:8186/QmsCloudApi/v1_1/address/getProvinceList';
        this.urlDists = 'http://222.255.102.157:8186/QmsCloudApi/v1_1/address/getDistrictList/';
        this.keyProvinces = 'provinces';
        this.keyDists = 'districts';
        this.getLocation();
        this.loadProvinces();
    }
    MapPage.prototype.getProvinces = function () {
        var _this = this;
        var data;
        var headersObj = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]({ 'API_KEY': '6358EBEA574E98FF5405B59EF130AA06' });
        data = this.http.get(this.urlProvinces, { headers: headersObj });
        data.subscribe(function (result) {
            _this.storage.set(_this.keyProvinces, JSON.stringify(result.data.provinceList)).then(function () {
                _this.loadProvinces();
            });
        });
        console.log('getprovinces');
    };
    MapPage.prototype.loadProvinces = function () {
        var _this = this;
        this.storage.get(this.keyProvinces).then(function (value) {
            if (value != null && value != undefined) {
                _this.provinces = JSON.parse(value);
            }
            else {
                _this.getProvinces();
            }
        });
        console.log('loadprovinces');
    };
    MapPage.prototype.loadDists = function (provinceID) {
        var _this = this;
        this.getProvinceByID(provinceID);
        this.dists = null;
        var data;
        var headersObj = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]({ 'API_KEY': '6358EBEA574E98FF5405B59EF130AA06' });
        data = this.http.get(this.urlDists + provinceID, { headers: headersObj });
        data.subscribe(function (result) {
            _this.dists = result.data.districtList;
            _this.storage.set(_this.keyDists, JSON.stringify(result.data.districtList));
        });
        console.log('loadDists');
    };
    MapPage.prototype.loadMap = function () {
        var _this = this;
        this.map = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["c" /* GoogleMaps */].create('map2');
        this.map.one(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MAP_READY).then(function () {
            _this.locate();
        });
    };
    MapPage.prototype.locate = function () {
        // Add a marker
        var marker = this.map.addMarkerSync({
            'position': {
                "lat": this.lat,
                "lng": this.lng
            }
        });
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* Geocoder */].geocode({
            "position": {
                "lat": this.lat,
                "lng": this.lng
            }
        }).then(function (results) {
            var address = "V\u1ECB tr\u00ED hi\u1EC7n t\u1EA1i<br/>\n        To\u1EA1 \u0111\u1ED9:<br/>\n        Kinh \u0111\u1ED9: " + results[0].position.lat + "<br/>\n        V\u0129 \u0111\u1ED9: " + results[0].position.lng;
            marker.setTitle(address);
            marker.showInfoWindow();
        });
        // Move to the position
        this.map.animateCamera({
            'target': marker.getPosition(),
            'zoom': 17
        }).then(function () {
            marker.showInfoWindow();
        });
    };
    MapPage.prototype.reLocate = function (distName) {
        var _this = this;
        console.log('dist name', distName);
        var provinceName = this.province.provinceName;
        var address = distName + ", " + provinceName + ", Vi\u1EC7t Nam";
        console.log('address', address);
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* Geocoder */].geocode({
            "address": address
        }).then(function (results) {
            console.log(results[0]);
            // Add a marker
            var marker = _this.map.addMarkerSync({
                'position': results[0].position,
                'title': "Qu\u1EADn/Huy\u1EC7n: " + distName + "<br/>\n        T\u1EC9nh/Th\u00E0nh ph\u1ED1: " + provinceName + "<br/>\n          To\u1EA1 \u0111\u1ED9:<br/>\n          Kinh \u0111\u1ED9: " + results[0].position.lat + "<br/>\n          V\u0129 \u0111\u1ED9: " + results[0].position.lng
            });
            // Move to the position
            _this.map.animateCamera({
                'target': marker.getPosition(),
                'zoom': 17
            }).then(function () {
                marker.showInfoWindow();
            });
        });
    };
    MapPage.prototype.getLocation = function () {
        var _this = this;
        this.geo.getCurrentPosition().then(function (pos) {
            _this.lat = pos.coords.latitude;
            _this.lng = pos.coords.longitude;
        }).catch(function (err) { return console.log(err); });
    };
    MapPage.prototype.getProvinceByID = function (provinceId) {
        var _this = this;
        this.storage.get(this.keyProvinces).then(function (value) {
            var provinces = JSON.parse(value);
            for (var i = 0; i < provinces.length; i++) {
                if (provinces[i].provinceId == provinceId) {
                    _this.province = provinces[i];
                }
            }
        });
    };
    MapPage.prototype.getDistByID = function (distId) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.storage.get(_this.keyDists).then(function (value) {
                var dists = JSON.parse(value);
                for (var i = 0; i < dists.length; i++) {
                    if (dists[i].districtId == distId) {
                        _this.dist = dists[i];
                    }
                }
                resolve();
            });
        });
        return promise;
    };
    /*moveCamera(loc: LatLng) {
      let options: CameraPosition = {
        target: loc,
        zoom: 15,
        tilt: 10
      }
      this.map.moveCamera(options);
    }*/
    MapPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MapPage');
    };
    MapPage.prototype.ionViewDidEnter = function () {
        this.loadMap();
        console.log('ionViewDidEnter MapPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], MapPage.prototype, "mapElement", void 0);
    MapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-map',template:/*ion-inline-start:"/Users/huutoan94/Ionic_projects/ionicTraining1/toan_training_project/src/pages/map/map.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons start>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Map</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-list>\n    <ion-item>\n      <ion-label>Tỉnh/Thành phố</ion-label>\n      <ion-select [(ngModel)]="provinceID" (ionChange)="loadDists(provinceID)">\n        <ion-option *ngFor="let province of provinces" value="{{province.provinceId}}">{{province.provinceName}}\n        </ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item *ngIf="dists != null">\n      <ion-label>Quận/Huyện</ion-label>\n      <ion-select [(ngModel)]="distName" (ionChange)="reLocate(distName)">\n        <ion-option *ngFor="let dist of dists" value="{{dist.districtName}}">{{dist.districtName}}</ion-option>\n      </ion-select>\n    </ion-item>\n  </ion-list>\n  <div id="map2" style=" width: 100%;\n    height: 80%;   ">\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/huutoan94/Ionic_projects/ionicTraining1/toan_training_project/src/pages/map/map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], MapPage);
    return MapPage;
}());

//# sourceMappingURL=map.js.map

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PeoplePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__details_details__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__map_map__ = __webpack_require__(290);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the PeoplePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PeoplePage = /** @class */ (function () {
    function PeoplePage(navCtrl, navParams, http, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.storage = storage;
        this.peoples = [];
        this.url = 'http://dummy.restapiexample.com/api/v1/employees';
        this.key = 'peoples';
        this.loadData();
    }
    PeoplePage.prototype.getData = function () {
        var _this = this;
        var data;
        var peoplesArray;
        data = this.http.get(this.url);
        data.subscribe(function (result) {
            peoplesArray = result.slice(0, 10);
            _this.storage.set(_this.key, JSON.stringify(peoplesArray)).then(function () {
                _this.loadData();
            });
        });
        console.log('getdata');
    };
    PeoplePage.prototype.loadData = function () {
        var _this = this;
        this.storage.get(this.key).then(function (value) {
            if (value != null && value != undefined) {
                _this.peoples = JSON.parse(value);
            }
            else {
                _this.getData();
            }
        });
        console.log('loaddata');
    };
    PeoplePage.prototype.openDetails = function (people) {
        console.log(people);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__details_details__["a" /* DetailsPage */], { people: people });
        //this.navCtrl.parent.select(1);
    };
    PeoplePage.prototype.refreshData = function () {
        var _this = this;
        this.storage.remove(this.key).then(function () {
            _this.loadData();
        });
    };
    PeoplePage.prototype.goToMap = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__map_map__["a" /* MapPage */]);
    };
    PeoplePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PeoplePage');
    };
    PeoplePage.prototype.ionViewWillEnter = function () {
        this.loadData();
    };
    PeoplePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-people',template:/*ion-inline-start:"/Users/huutoan94/Ionic_projects/ionicTraining1/toan_training_project/src/pages/people/people.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons start>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Peoples</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n    <ion-item *ngFor="let people of peoples" (click)="openDetails(people)">\n      <ion-avatar item-start>\n        <ion-icon name="contact"></ion-icon>\n      </ion-avatar>\n      <h2>{{people.employee_name}}</h2>\n      <p>{{people.employee_salary}}</p>\n    </ion-item>\n  </ion-list>\n  <button padding ion-button color="light" round (click)=\'refreshData()\'>Tải lại dữ liệu</button>\n</ion-content>'/*ion-inline-end:"/Users/huutoan94/Ionic_projects/ionicTraining1/toan_training_project/src/pages/people/people.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], PeoplePage);
    return PeoplePage;
}());

//# sourceMappingURL=people.js.map

/***/ })

});
//# sourceMappingURL=0.js.map