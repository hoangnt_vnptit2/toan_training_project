webpackJsonp([5],{

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPageModule", function() { return MapPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__map__ = __webpack_require__(290);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MapPageModule = /** @class */ (function () {
    function MapPageModule() {
    }
    MapPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__map__["a" /* MapPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__map__["a" /* MapPage */]),
            ],
        })
    ], MapPageModule);
    return MapPageModule;
}());

//# sourceMappingURL=map.module.js.map

/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(201);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MapPage = /** @class */ (function () {
    function MapPage(navCtrl, navParams, http, geo, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.geo = geo;
        this.storage = storage;
        this.provinces = [];
        this.province = {};
        this.dist = {};
        this.urlProvinces = 'http://222.255.102.157:8186/QmsCloudApi/v1_1/address/getProvinceList';
        this.urlDists = 'http://222.255.102.157:8186/QmsCloudApi/v1_1/address/getDistrictList/';
        this.keyProvinces = 'provinces';
        this.keyDists = 'districts';
        this.getLocation();
        this.loadProvinces();
    }
    MapPage.prototype.getProvinces = function () {
        var _this = this;
        var data;
        var headersObj = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]({ 'API_KEY': '6358EBEA574E98FF5405B59EF130AA06' });
        data = this.http.get(this.urlProvinces, { headers: headersObj });
        data.subscribe(function (result) {
            _this.storage.set(_this.keyProvinces, JSON.stringify(result.data.provinceList)).then(function () {
                _this.loadProvinces();
            });
        });
        console.log('getprovinces');
    };
    MapPage.prototype.loadProvinces = function () {
        var _this = this;
        this.storage.get(this.keyProvinces).then(function (value) {
            if (value != null && value != undefined) {
                _this.provinces = JSON.parse(value);
            }
            else {
                _this.getProvinces();
            }
        });
        console.log('loadprovinces');
    };
    MapPage.prototype.loadDists = function (provinceID) {
        var _this = this;
        this.getProvinceByID(provinceID);
        this.dists = null;
        var data;
        var headersObj = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]({ 'API_KEY': '6358EBEA574E98FF5405B59EF130AA06' });
        data = this.http.get(this.urlDists + provinceID, { headers: headersObj });
        data.subscribe(function (result) {
            _this.dists = result.data.districtList;
            _this.storage.set(_this.keyDists, JSON.stringify(result.data.districtList));
        });
        console.log('loadDists');
    };
    MapPage.prototype.loadMap = function () {
        var _this = this;
        this.map = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["c" /* GoogleMaps */].create('map2');
        this.map.one(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MAP_READY).then(function () {
            _this.locate();
        });
    };
    MapPage.prototype.locate = function () {
        // Add a marker
        var marker = this.map.addMarkerSync({
            'position': {
                "lat": this.lat,
                "lng": this.lng
            }
        });
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* Geocoder */].geocode({
            "position": {
                "lat": this.lat,
                "lng": this.lng
            }
        }).then(function (results) {
            var address = "V\u1ECB tr\u00ED hi\u1EC7n t\u1EA1i<br/>\n        To\u1EA1 \u0111\u1ED9:<br/>\n        Kinh \u0111\u1ED9: " + results[0].position.lat + "<br/>\n        V\u0129 \u0111\u1ED9: " + results[0].position.lng;
            marker.setTitle(address);
            marker.showInfoWindow();
        });
        // Move to the position
        this.map.animateCamera({
            'target': marker.getPosition(),
            'zoom': 17
        }).then(function () {
            marker.showInfoWindow();
        });
    };
    MapPage.prototype.reLocate = function (distName) {
        var _this = this;
        console.log('dist name', distName);
        var provinceName = this.province.provinceName;
        var address = distName + ", " + provinceName + ", Vi\u1EC7t Nam";
        console.log('address', address);
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* Geocoder */].geocode({
            "address": address
        }).then(function (results) {
            console.log(results[0]);
            // Add a marker
            var marker = _this.map.addMarkerSync({
                'position': results[0].position,
                'title': "Qu\u1EADn/Huy\u1EC7n: " + distName + "<br/>\n        T\u1EC9nh/Th\u00E0nh ph\u1ED1: " + provinceName + "<br/>\n          To\u1EA1 \u0111\u1ED9:<br/>\n          Kinh \u0111\u1ED9: " + results[0].position.lat + "<br/>\n          V\u0129 \u0111\u1ED9: " + results[0].position.lng
            });
            // Move to the position
            _this.map.animateCamera({
                'target': marker.getPosition(),
                'zoom': 17
            }).then(function () {
                marker.showInfoWindow();
            });
        });
    };
    MapPage.prototype.getLocation = function () {
        var _this = this;
        this.geo.getCurrentPosition().then(function (pos) {
            _this.lat = pos.coords.latitude;
            _this.lng = pos.coords.longitude;
        }).catch(function (err) { return console.log(err); });
    };
    MapPage.prototype.getProvinceByID = function (provinceId) {
        var _this = this;
        this.storage.get(this.keyProvinces).then(function (value) {
            var provinces = JSON.parse(value);
            for (var i = 0; i < provinces.length; i++) {
                if (provinces[i].provinceId == provinceId) {
                    _this.province = provinces[i];
                }
            }
        });
    };
    MapPage.prototype.getDistByID = function (distId) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.storage.get(_this.keyDists).then(function (value) {
                var dists = JSON.parse(value);
                for (var i = 0; i < dists.length; i++) {
                    if (dists[i].districtId == distId) {
                        _this.dist = dists[i];
                    }
                }
                resolve();
            });
        });
        return promise;
    };
    /*moveCamera(loc: LatLng) {
      let options: CameraPosition = {
        target: loc,
        zoom: 15,
        tilt: 10
      }
      this.map.moveCamera(options);
    }*/
    MapPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MapPage');
    };
    MapPage.prototype.ionViewDidEnter = function () {
        this.loadMap();
        console.log('ionViewDidEnter MapPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], MapPage.prototype, "mapElement", void 0);
    MapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-map',template:/*ion-inline-start:"/Users/huutoan94/Ionic_projects/ionicTraining1/toan_training_project/src/pages/map/map.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons start>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Map</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-list>\n    <ion-item>\n      <ion-label>Tỉnh/Thành phố</ion-label>\n      <ion-select [(ngModel)]="provinceID" (ionChange)="loadDists(provinceID)">\n        <ion-option *ngFor="let province of provinces" value="{{province.provinceId}}">{{province.provinceName}}\n        </ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item *ngIf="dists != null">\n      <ion-label>Quận/Huyện</ion-label>\n      <ion-select [(ngModel)]="distName" (ionChange)="reLocate(distName)">\n        <ion-option *ngFor="let dist of dists" value="{{dist.districtName}}">{{dist.districtName}}</ion-option>\n      </ion-select>\n    </ion-item>\n  </ion-list>\n  <div id="map2" style=" width: 100%;\n    height: 80%;   ">\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/huutoan94/Ionic_projects/ionicTraining1/toan_training_project/src/pages/map/map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], MapPage);
    return MapPage;
}());

//# sourceMappingURL=map.js.map

/***/ })

});
//# sourceMappingURL=5.js.map