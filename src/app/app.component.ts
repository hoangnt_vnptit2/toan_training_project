import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Environment } from '@ionic-native/google-maps';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      
      this.rootPage = 'MenuPage'
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      Environment.setEnv({
        'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCNGbFeXfIUXNjEX_T38nvUHS0l3WbsLjk',
        'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCNGbFeXfIUXNjEX_T38nvUHS0l3WbsLjk'
      });
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

