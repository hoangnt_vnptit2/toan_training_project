import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  peopleRoot: any = 'PeoplePage';
  detailsRoot: any = 'DetailsPage';
  mapRoot: any = 'MapPage';
  myIndex: number;

  constructor(navParams: NavParams) {
    this.myIndex = navParams.data.tabIndex || 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }
}
