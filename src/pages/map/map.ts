import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ViewChild, ElementRef } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';

import { GoogleMaps, GoogleMap, Environment, Marker, CameraPosition, LatLng, GoogleMapsEvent, Geocoder, GeocoderResult, GoogleMapOptions } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  @ViewChild('map') mapElement: ElementRef;
  map: GoogleMap;
  lat: any;
  lng: any;

  provinces: any = [];
  dists: any;
  province: any = {};
  dist: any = {};
  urlProvinces: string = 'http://222.255.102.157:8186/QmsCloudApi/v1_1/address/getProvinceList';
  urlDists: string = 'http://222.255.102.157:8186/QmsCloudApi/v1_1/address/getDistrictList/'
  keyProvinces: string = 'provinces';
  keyDists: string = 'districts';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpClient,
    public geo: Geolocation,
    private storage: Storage) {
    this.getLocation();
    this.loadProvinces();
  }

  getProvinces() {
    let data: Observable<any>;
    let headersObj = new HttpHeaders({ 'API_KEY': '6358EBEA574E98FF5405B59EF130AA06' });
    data = this.http.get(this.urlProvinces, { headers: headersObj });
    data.subscribe(result => {
      this.storage.set(this.keyProvinces, JSON.stringify(result.data.provinceList)).then(() => {
        this.loadProvinces();
      });
    });
    console.log('getprovinces');
  }

  loadProvinces() {
    this.storage.get(this.keyProvinces).then(value => {
      if (value != null && value != undefined) {
        this.provinces = JSON.parse(value);
      } else {
        this.getProvinces();
      }
    })
    console.log('loadprovinces');
  }

  loadDists(provinceID) {
    this.getProvinceByID(provinceID);
    this.dists = null;
    let data: Observable<any>;
    let headersObj = new HttpHeaders({ 'API_KEY': '6358EBEA574E98FF5405B59EF130AA06' });
    data = this.http.get(this.urlDists + provinceID, { headers: headersObj });
    data.subscribe(result => {
      this.dists = result.data.districtList;
      this.storage.set(this.keyDists, JSON.stringify(result.data.districtList));
    });
    console.log('loadDists');
  }

  loadMap() {
    this.map = GoogleMaps.create('map2');
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      this.locate();
    });
  }

  locate() {
    // Add a marker
    let marker: Marker = this.map.addMarkerSync({
      'position': {
        "lat": this.lat,
        "lng": this.lng
      }
    });

    Geocoder.geocode({
      "position": {
        "lat": this.lat,
        "lng": this.lng
      }
    }).then((results: GeocoderResult[]) => {
      let address: any = `Vị trí hiện tại
        Toạ độ:
        Kinh độ: ${results[0].position.lat}
        Vĩ độ: ${results[0].position.lng}`

      marker.setTitle(address);
      marker.showInfoWindow();
    });

    // Move to the position
    this.map.animateCamera({
      'target': marker.getPosition(),
      'zoom': 17
    }).then(() => {
      marker.showInfoWindow();
    });
  }

  reLocate(distName) {
    console.log('dist name', distName);
    let provinceName = this.province.provinceName;
    let address: string = `${distName}, ${provinceName}, Việt Nam`;
    console.log('address', address);
    Geocoder.geocode({
      "address": address
    }).then((results: GeocoderResult[]) => {
      console.log(results[0]);

      // Add a marker
      let marker: Marker = this.map.addMarkerSync({
        'position': results[0].position,
        'title': `Quận/Huyện: ${distName}
        Tỉnh/Thành phố: ${provinceName}
          Toạ độ:
          Kinh độ: ${results[0].position.lat}
          Vĩ độ: ${results[0].position.lng}`
      });

      // Move to the position
      this.map.animateCamera({
        'target': marker.getPosition(),
        'zoom': 17
      }).then(() => {
        marker.showInfoWindow();
      });
    });
  }

  getLocation() {
    this.geo.getCurrentPosition().then(pos => {
      this.lat = pos.coords.latitude;
      this.lng = pos.coords.longitude;
    }).catch(err => console.log(err));
  }

  getProvinceByID(provinceId) {
    this.storage.get(this.keyProvinces).then(value => {
      let provinces = JSON.parse(value);
      for (let i = 0; i < provinces.length; i++) {
        if (provinces[i].provinceId == provinceId) {
          this.province = provinces[i];
        }
      }
    })
  }

  getDistByID(distId) {
    var promise = new Promise((resolve, reject) => {
      this.storage.get(this.keyDists).then(value => {
        let dists = JSON.parse(value);
        for (let i = 0; i < dists.length; i++) {
          if (dists[i].districtId == distId) {
            this.dist = dists[i];
          }
        }
        resolve();
      })
    });
    return promise;
  }

  /*moveCamera(loc: LatLng) {
    let options: CameraPosition = {
      target: loc,
      zoom: 15,
      tilt: 10
    }
    this.map.moveCamera(options);
  }*/

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
  }

  ionViewDidEnter() {
    this.loadMap();
    console.log('ionViewDidEnter MapPage');
  }
}
