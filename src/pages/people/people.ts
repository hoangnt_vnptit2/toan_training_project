import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';

import { DetailsPage} from '../details/details';
import { MapPage } from '../map/map';

/**
 * Generated class for the PeoplePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-people',
  templateUrl: 'people.html',
})
export class PeoplePage {

  peoples: any = [];
  url: string = 'http://dummy.restapiexample.com/api/v1/employees';
  key: string = 'peoples';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpClient,
    private storage: Storage
  ) {
    this.loadData();
  }

  getData() {
    let data: Observable<any>;
    let peoplesArray: Array<any>;
    data = this.http.get(this.url);
    data.subscribe(result => {
      peoplesArray = result.slice(0,10);
      this.storage.set(this.key, JSON.stringify(peoplesArray)).then(() => {
        this.loadData();
      });
    });
    console.log('getdata');
  }

  loadData() {
    this.storage.get(this.key).then(value => {
      if (value != null && value != undefined) {
        this.peoples = JSON.parse(value);
      } else {
        this.getData();
      }
    })
    console.log('loaddata');
  }

  openDetails(people) {
    console.log(people);
    this.navCtrl.push(DetailsPage, {people:people});
    //this.navCtrl.parent.select(1);
  }

  refreshData() {
    this.storage.remove(this.key).then(() => {
      this.loadData();
    });
  }

  goToMap() {
    this.navCtrl.push(MapPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PeoplePage');
  }

  ionViewWillEnter() {
    this.loadData()
  }

}