import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
//import { PeoplePage } from '../people/people';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()

@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {

  people: any;
  peoples: any = [];
  key: string = 'peoples';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    public alertCtrl: AlertController) {
    this.people = this.navParams.get('people');
  }

  deleteConfirm() {
    const confirm = this.alertCtrl.create({
      title: 'Xoá nhân viên',
      message: `Bạn có thực sự muốn xoá nhân viên khỏi danh sách ?`,
      buttons: [
        {
          text: 'Không',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Có',
          handler: () => {
            this.presentLoading();
            this.deletePeople();
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ ...",
      duration: 2000
    });
    loader.present();
  }

  deletePeople() {
    this.storage.get(this.key).then(value => {
      if (value != null && value != undefined) {
        this.peoples = JSON.parse(value);
        for (let i = 0; i < this.peoples.length; i++) {
          if (this.peoples[i].id === this.people.id) {
            this.peoples.splice(i, 1);
          }
        }
        this.storage.set(this.key, JSON.stringify(this.peoples));
      }
    })
    this.navCtrl.pop();
    //this.navCtrl.parent.select(0);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');
  }

}
